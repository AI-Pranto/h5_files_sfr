from multiprocessing import Pool
import glob
import openmc.data

neutron_files = glob.glob('*.dat')

library = openmc.data.DataLibrary()

# for filename in sorted(neutron_files):
#     data = openmc.data.IncidentNeutron.from_njoy(filename, temperatures= [600.0, 900.0, 1200.0, 2500.0], stdout=True)
#     h5_file = f'{data.name}.h5'
#     print(f'Writing {h5_file} ...')
#     data.export_to_hdf5(h5_file, 'w', libver= 'latest')

def process_neutron(path, temperatures=None):
    data = openmc.data.IncidentNeutron.from_njoy(path, temperatures=temperatures, stdout=True)

    h5_file = f'{data.name}.h5'
    print(f'Writing {h5_file} ...')
    data.export_to_hdf5(h5_file, 'w', libver= 'latest')

with Pool() as pool:
    results = []
    for filename in sorted(neutron_files):
        func_args = (filename, [600, 900, 1500, 3000])
        r = pool.apply_async(process_neutron, func_args)
        results.append(r)

    for r in results:
        r.wait()


# Register with library
for p in sorted(glob.glob('*.h5')):
    library.register_file(p)

# Write cross_sections.xml
library.export_to_xml('cross_sections.xml')
